import { useState } from "react";

function Input(props) {
    const { labelValue, ...inputProps } = props;
    const [count, setCount] = useState('');

    return (
        <div className="form-group">
            {labelValue && <label className={'me-1'}>{labelValue}</label>}

            <input {...inputProps} />
        </div>
    );
}

export default Input;