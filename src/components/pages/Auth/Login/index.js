import classNames from 'classnames/bind';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';
import { useSelector, useDispatch } from 'react-redux';

import { login } from '../../../../services/authService';

import styles from './Login.module.scss';
import { Link, Navigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { setAuthAction } from '../../../../store/actions/setAuthAction';
import { useNavigate } from 'react-router-dom';
import actionTypes from '../../../../store/actions/actionType';
const cx = classNames.bind(styles);

function Login() {
    const dispatch = useDispatch();
    const { isAuthenticated } = useSelector((state) => state.auth);
    const navigate = useNavigate();

    const [value, setValue] = useState({
        email: '',
        password: '',
    });

    const [errMessage, setErrmessage] = useState('');

    useEffect(() => {
        const hidenErrmessage = setTimeout(() => {
            setErrmessage('');
        }, 3000);

        return () => {
            clearTimeout(hidenErrmessage);
        };
    }, [errMessage]);

    // useEffect(()=>{
    //     if(isAuthenticated) navigate('/')
    // },[isAuthenticated])

    const handleChange = (event) => {
        setValue({
            ...value,
            [event.target.name]: event.target.value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
         dispatch({
             type: actionTypes.AUTH_LOADING,
             payload: {
                 authLoading: true,
             },
         });

        if (value.email && value.password) {
            const data = {
                email: value.email,
                password: value.password,
            };
            try {
                const loginData = await login(data);
                if (!loginData.success) {
                    setErrmessage('Email hoặc mật khẩu không đúng');
                } else {
                    dispatch(setAuthAction());
                    // navigate('/');
                }
            } catch (error) {
                console.log(error);
            }
        } else {
            setErrmessage('Vui lòng nhập các trường còn thiếu');
        }
    };

    const errmessage = (
        <div style={{ color: 'red', fontSize: '1.8rem' }}>
            <span>{errMessage}</span>
        </div>
    );

    // if (isAuthenticated) {
    //     return <Navigate to={'/'} />;
    // }

    return (
        <div className={cx('wrapper')}>
            <div className={cx('title')}>
                <span className={cx('icon')}>
                    <FontAwesomeIcon icon={faUser} />
                </span>

                <h5>User login</h5>
                <h6>
                    Tài khoản của bạn luôn được bảo mật và được mã hóa trên hệ thống của chúng tôi
                </h6>
            </div>
            <hr />

            <form method="POST" className={cx('main-form')}>
                {errmessage}
                {/* {errmessage}
                {status && name} */}
                <div className={cx('form-group')}>
                    <label>Email</label>
                    <div className={cx('form-control')}>
                        <span>
                            <FontAwesomeIcon icon={faUser} />
                        </span>
                        <input
                            value={value.email}
                            onChange={(e) => {
                                handleChange(e);
                            }}
                            required
                            type={'email'}
                            name="email"
                            placeholder="Enter Email"
                        />
                    </div>
                </div>
                <div className={cx('form-group')}>
                    <label>Password</label>
                    <div className={cx('form-control')}>
                        <span>
                            <FontAwesomeIcon icon={faLock} />
                        </span>
                        <input
                            value={value.password}
                            onChange={(e) => {
                                handleChange(e);
                            }}
                            required
                            type={'password'}
                            name="password"
                            placeholder="Enter Password"
                        />
                    </div>
                </div>

                <div className={cx('btn', 'active')}>
                    <button
                        onClick={(e) => {
                            handleSubmit(e);
                        }}
                        type="submit"
                    >
                        {'Đăng nhập'}
                    </button>
                </div>
                <div className={cx('btn', 'toggel')}>
                    <p>{'Bạn chưa có tài khoản? Vui lòng đăng ký'}</p>

                    <Link to={'/register'}>
                        <button>{'Đăng ký'}</button>
                    </Link>
                </div>
            </form>
        </div>
    );
}

export default Login;
