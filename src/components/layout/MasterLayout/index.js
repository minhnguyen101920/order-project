import { useSelector } from 'react-redux';
import Header from '../../component/Header';
import { Navigate } from 'react-router-dom';

function MasterLayout({ children }) {
    const { isAuthenticated, authLoading } = useSelector((state) => state.auth);

    

    if (authLoading) return <h1>Loading</h1>;
    else if (!isAuthenticated) {
        return <Navigate to={'/login'} />;
    } else
        return (
            <div>
                <Header />
                {children}
                <h1>Footer</h1>
            </div>
        );
}

export default MasterLayout;
