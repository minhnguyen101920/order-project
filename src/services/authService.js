import axios from 'axios';

// import axiosInstance from '../config/axios';
import setAuthToken from '../utils/setAuthToken';

async function register(user) {
    try {
        const response = await axios.post(
            `${process.env.REACT_APP_API_URL}/api/auth/register`,
            user,
        );
        return response.data;
    } catch (error) {
        if (error.response.data) return error.response.data;
        return { success: false, message: error.message };
    }
}

async function login(user) {
    try {
        const response = await axios.post(`${process.env.REACT_APP_API_URL}/api/auth/login`, user);

        if (response.data.success) {
            localStorage.setItem('AccsessToken', response.data.token);
        }
        return response.data;
    } catch (error) {
        if (error.response.data) return error.response.data;
        return { success: false, message: error.message };
    }
}

async function loadUser() {
    const token = localStorage.getItem('AccsessToken');
    setAuthToken(token)
    try {
        const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/auth`);
        return response.data;
    } catch (error) {
        if (error.response.data) return error.response.data;
        return { success: false, message: error.message };
    }
}

export { register, login, loadUser };
