import axios from 'axios';

// Tạo một instance Axios mới
const axiosInstance = axios.create();

// Hàm để set Token vào header
const setAuthTokens = (token) => {
    if (token) {
        // Thiết lập Authorization header với giá trị Token
        axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    } else {
        // Nếu không có Token, xóa Authorization header
        delete axiosInstance.defaults.headers.common['Authorization'];
    }
};


const setAuthToken = (token) => {
    axiosInstance.interceptors.request.use(
        (config) => {
            // const token = localStorage.getItem('AccsessToken');
            config.headers['Authorization'] = `Bearer ${token}`;
            return config;
        },
        (error) => {
            return Promise.reject(error);
        },
    );
};

export { setAuthToken };

export default axiosInstance;
