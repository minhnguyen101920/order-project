import classNames from 'classnames/bind';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faLock, faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons';

import styles from './Register.module.scss';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { register } from '../../../../services/authService';

const cx = classNames.bind(styles);

function Register() {
    const navigate = useNavigate();

    const [errMessage, setErrmessage] = useState('');

    const [value, setValue] = useState({
        username: '',
        email: '',
        password: '',
        confirmpassword: '',
        phone: '',
    });

    const handleChange = (event) => {
        setValue({
            ...value,
            [event.target.name]: event.target.value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (
            !value.email ||
            !value.password ||
            !value.confirmpassword ||
            !value.username ||
            !value.phone
        ) {
            setErrmessage('Vui lòng nhập các trường còn thiếu');
            return;
        }

        if (value.password !== value.confirmpassword) {
            setErrmessage('Mật khẩu không trùng nhau');
            return;
        } else {
            const data = {
                email: value.email,
                password: value.password,
                username: value.username,
                phone: value.phone,
            };
            try {
                const registerData = await register(data);
                if (!registerData.success) {
                    setErrmessage('Tài khoản này đã tồn tại');
                    return;
                }
                navigate('/login');
            } catch (error) {
                console.log(error);
            }
        }
    };

    const errmessage = (
        <div style={{ color: 'red', fontSize: '1.8rem' }}>
            <span>{errMessage}</span>
        </div>
    );

    return (
        <div className={cx('wrapper')}>
            <div className={cx('title')}>
                <h5>Đăng ký</h5>
                <h6>Đăng ký và trải nghiệm sản phẩm của chúng tôi</h6>
            </div>
            <hr />

            <form method="POST" className={cx('main-form')}>
                {errmessage}
                <div className={cx('form-group')}>
                    <label>Username</label>
                    <div className={cx('form-control')}>
                        <span>
                            <FontAwesomeIcon icon={faUser} />
                        </span>
                        <input
                            required
                            type={'text'}
                            name="username"
                            placeholder="Enter Username"
                            value={value.username}
                            onChange={(e) => {
                                handleChange(e);
                            }}
                        />
                    </div>
                </div>
                <div className={cx('form-group')}>
                    <label>Email</label>
                    <div className={cx('form-control')}>
                        <span>
                            <FontAwesomeIcon icon={faEnvelope} />
                        </span>
                        <input
                            value={value.email}
                            onChange={(e) => {
                                handleChange(e);
                            }}
                            required
                            type={'email'}
                            name="email"
                            placeholder="Enter Email"
                        />
                    </div>
                </div>
                <div className={cx('form-group')}>
                    <label>Phone</label>
                    <div className={cx('form-control')}>
                        <span>
                            <FontAwesomeIcon icon={faPhone} />
                        </span>
                        <input
                            value={value.phoneNumber}
                            onChange={(e) => {
                                handleChange(e);
                            }}
                            required
                            type={'text'}
                            name="phone"
                            placeholder="Enter phone Number"
                        />
                    </div>
                </div>
                <div className={cx('form-group')}>
                    <label>Password</label>
                    <div className={cx('form-control')}>
                        <span>
                            <FontAwesomeIcon icon={faLock} />
                        </span>
                        <input
                            value={value.password}
                            onChange={(e) => {
                                handleChange(e);
                            }}
                            required
                            type={'password'}
                            name="password"
                            placeholder="Enter Password"
                        />
                    </div>
                </div>

                <div className={cx('form-group')}>
                    <label>Confirmpassword</label>
                    <div className={cx('form-control')}>
                        <span>
                            <FontAwesomeIcon icon={faLock} />
                        </span>
                        <input
                            value={value.confirmpassword}
                            onChange={(e) => {
                                handleChange(e);
                            }}
                            required
                            type={'password'}
                            name="confirmpassword"
                            placeholder="Enter Confirmpassword"
                        />
                    </div>
                </div>
                <div className={cx('btn', 'active')}>
                    <button
                        onClick={(e) => {
                            handleSubmit(e);
                        }}
                        type="submit"
                    >
                        {'Đăng ký'}
                    </button>
                </div>
                <div className={cx('btn', 'toggel')}>
                    <p>{'Bạn đã có tài khoản? Vui lòng đăng nhập'}</p>

                    <Link to={'/login'}>
                        <button>{'Đăng nhập'}</button>
                    </Link>
                </div>
            </form>
        </div>
    );
}

export default Register;
