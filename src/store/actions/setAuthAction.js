// import axios from "axios";
// import setAuthToken from "../../utils/setAuthToken";
import actionTypes from './actionType';
import { loadUser } from '../../services/authService';
import { setAuthToken } from '../../config/axios';

export const setAuthAction = () => async (dispatch) => {
    const token = localStorage.getItem('AccsessToken');
    setAuthToken(token)

    try {
        const response = await loadUser();
        // console.log(response);
        if (response.success) {
            dispatch({
                type: actionTypes.SET_AUTH,
                payload: {
                    authLoading: false,
                    isAuthenticated: true,
                    user: response.user,
                },
            });
        }else{
            dispatch({
                type: actionTypes.SET_AUTH,
                payload: {
                    authLoading: false,
                    isAuthenticated: false,
                    user: null,
                },
            });
        }
    } catch (error) {
        localStorage.removeItem('AccsessToken');
        dispatch({
            type: actionTypes.SET_AUTH,
            payload: {
                authLoading: false,
                isAuthenticated: false,
                user: null,
            },
        });
    }
};
