import { Fragment, useEffect } from 'react';
// import Button from '../src/components/component/Button/index';
import { webRoutes,  authRoutes } from './routes';
import { Routes, Route  } from 'react-router-dom';
import MasterLayout from './components/layout/MasterLayout';
import { useDispatch, useSelector } from 'react-redux';
import { setAuthAction } from './store/actions/setAuthAction';
import actionTypes from './store/actions/actionType';
import { Admin, EditGuesser, Resource } from 'react-admin';
import UserList from './components/pages/Admin/ListUser';
import { dataProvider } from './consts/dataProvider';

function App() {
    const { isAuthenticated } = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    // const navigate = useNavigate();
    useEffect(() => {
        dispatch({
            type: actionTypes.AUTH_LOADING,
            payload: {
                authLoading: true,
            },
        });

        dispatch(setAuthAction());
        // if (!isAuthenticated) navigate('/login');
    }, [isAuthenticated]);

    // const routingAdmin = useRoutes(adminRoutes);

    return (
        <>
            <Routes>
                {authRoutes.map((route, key) => {
                    const Page = route.component;
                    let Layout;
                    if (route.layout) {
                        Layout = route.layout;
                    } else if (route.layout === null) {
                        Layout = Fragment;
                    }
                    return (
                        <Route
                            key={key}
                            path={route.path}
                            element={
                                <Layout>
                                    <Page />
                                </Layout>
                            }
                        />
                    );
                })}

                {webRoutes.map((route, key) => {
                    const Page = route.component;
                    let Layout = MasterLayout;
                    if (route.layout) {
                        Layout = route.layout;
                    } else if (route.layout === null) {
                        Layout = Fragment;
                    }
                    return (
                        <Route
                            key={key}
                            path={route.path}
                            element={
                                <Layout>
                                    <Page />
                                </Layout>
                            }
                        />
                    );
                })}

                <Route
                    path="admin/*"
                    element={
                        <Admin
                            basename="/admin"
                            dataProvider={dataProvider}
                            title={'Order Manager'}
                        >
                            <Resource name="users" list={UserList} edit={EditGuesser} />
                            <Resource name="order" list={UserList} edit={EditGuesser} />
                        </Admin>
                    }
                />
            </Routes>
        </>
    );
}

export default App;
