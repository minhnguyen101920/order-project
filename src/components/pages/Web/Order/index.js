import { useEffect, useState } from 'react';
import Button from '../../../component/Button';
import OrderItem from '../../../component/OrderItem';
import InfoOrder from '../../../component/InfoOrder';

function Order() {
    const [submit, setSubmit] = useState(false);
    const [orders, setOrder] = useState([]);
    const [showInfo, setShowInfo] = useState(false)

    useEffect(()=>{
        if (!orders.length) {
            setSubmit(false)
        }
    },[orders])

    const handleRemove = (index) => {
        setOrder((prevOrders) => {
            const newOrders = prevOrders.filter((item, i) => i !== index);
            return newOrders;
        });
    };

    const handleCreateOrder = () => {
        setOrder((prevOrders) => [...prevOrders, OrderItem]);
    };

    const handleShowInfo = ()=>{
        if(orders.length) setShowInfo(true)
        return;
    }

  

    return (
        <div className={'order-component row my-5'}>
            {orders.length > 0 &&
                orders.map((Item, index) => {
                   

                    return (
                        <Item
                            key={index}
                            _key={index}
                            submit={submit}
                            handleRemove={handleRemove}
                        />
                    );
                })}

            <InfoOrder show={showInfo} setShow={setShowInfo} setSubmit={setSubmit} />
            <div className="col-lg-4 offset-lg-8 mt-5">
                <Button
                    className="btn btn-outline-success"
                    type="submit"
                    onClick={handleShowInfo}
                >
                    Lưu đơn hàng
                </Button>
                <Button
                    className="mx-2 btn btn-outline-danger"
                    type="submit"
                    onClick={handleCreateOrder}
                >
                    Tạo đơn
                </Button>
            </div>
        </div>
    );
}

export default Order;
