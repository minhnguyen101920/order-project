import { NavLink } from 'react-router-dom';
import { DATA_MENU } from '../../../consts';
import './Style.scss';
import { FaSignOutAlt } from 'react-icons/fa';
import logo from '../../../assets/image/logo/Set_of_Cute_Delivery_logo_cartoon_art_illustration-01.png';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import actionTypes from '../../../store/actions/actionType';
import { setAuthAction } from '../../../store/actions/setAuthAction';

const Header = (props) => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const menu_data = DATA_MENU;
    const handleLogout = () => {
        localStorage.removeItem('AccsessToken');
        dispatch(setAuthAction());
    };

    return (
        <nav className="container d-flex justify-content-center align-items-center">
            <div>
                {menu_data &&
                    menu_data.length > 0 &&
                    menu_data.map((item, index) => {
                        // {
                        if (item.image == null) {
                            return (
                                <NavLink
                                    key={index}
                                    to={item.path}
                                    className="nav-item text-uppercase me-2"
                                >
                                    {' '}
                                    {item.name}
                                </NavLink>
                            );
                        } else {
                            return (
                                <NavLink key={index} to={item.path} className="nav-item me-2">
                                    {' '}
                                    <img className="" src={logo} alt="logo" width={150} />
                                </NavLink>
                            );
                        }
                    })}
            </div>
            <div>
                <FaSignOutAlt className="ms-4 btn-logout" onClick={handleLogout} />
            </div>
        </nav>
    );
};
export default Header;
