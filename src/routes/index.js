import FormLayout from '../components/layout/FormLayout';
import Home from '../components/pages/Web/Home';
import Login from '../components/pages/Auth/Login';
import Register from '../components/pages/Auth/Register';
// import HomeAdmin from '../components/pages/Admin/HomeAdmin';
import Order from '../components/pages/Web/Order';
import History from '../components/pages/Web/Hisrory';
import { Admin, ListGuesser, Resource } from 'react-admin';
import UserList from '../components/pages/Admin/ListUser';
import { dataProvider } from '../consts/dataProvider';

const authRoutes = [
    { path: '/login', component: Login, layout: FormLayout },
    { path: '/register', component: Register, layout: FormLayout },
    // { path: '/', component: Home },
];

const webRoutes = [
    { path: '/', component: Home },
    { path: '/order', component: Order },
    { path: '/history', component: History },
];

const adminRoutes = [
    {
        path: '/',
        element: (
            <Admin dataProvider={dataProvider}>
                <Resource path={'/users'} name="/users" list={UserList} />
            </Admin>
        ),
        children: [
            { index: true, element: <h1>Welcome to Admin Panel</h1> },
            { path: 'list', element: <Resource name="list" list={ListGuesser} /> },
        ],
    },
];

export { webRoutes, adminRoutes, authRoutes };
