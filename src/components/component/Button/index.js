import { Link } from "react-router-dom";
import classNames from "classnames/bind";
import styles from './Button.module.scss'

const cx = classNames.bind(styles);


function Button({
    to, 
    href,
    children,
    onClick,
    primary = false,
    outline = false,
    text = false,
    small = false,
    large = false,
    disabled = false,
    ...passProps}) {

    let Comp = 'button';
    const props = {
        to,
        href,  
        onClick,
        ...passProps
    }


    if(to){
        Comp = Link;
    }else if(href){
        Comp = 'a';
    }
    const classes = cx('wrapper' ,{
        primary,
        outline,
        text,
        small,
        large,
        disabled
    });
    return ( 
        <Comp className={classes} {...props}>
            <span>{children}</span>
        </Comp>
     );
}

export default Button;