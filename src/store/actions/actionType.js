const actionTypes = {

    SET_AUTH : 'SET_AUTH',
    AUTH_LOADING:'AUTH_LOADING'
}

export default actionTypes
