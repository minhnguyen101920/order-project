export const DATA_MENU = [
    { path: '/', name: 'Trang chủ', image: null },
    {
        path: '/order',
        name: 'Đặt hàng',
        image: 'Set_of_Cute_Delivery_logo_cartoon_art_illustration-01.png',
    },
    { path: '/history', name: 'Lịch sử', image: null },
];
