import axios from 'axios';

const upload = async (formData) => {
    try {
        const response = await axios.post('http://localhost:3333/uploads', formData, {
            headers: {
                'Content-Type': 'multipart/form-data', // Đặt kiểu nội dung là 'multipart/form-data'
            },
        });

        return response;
    } catch (error) {
        return false;
    }
};


export {upload}