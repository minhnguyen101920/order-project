const users = [
    { id: 1, name: 'xxx', email: 'john@example.com' },
    { id: 2, name: 'yyy', email: 'jane@example.com' },
    { id: 3, name: 'yyy', email: 'jane@example.com' },
    { id: 4, name: 'yyy', email: 'jane@example.com' },
    { id: 5, name: 'yyy', email: 'jane@example.com' },
    { id: 6, name: 'yyy', email: 'jane@example.com' },
    { id: 7, name: 'yyy', email: 'jane@example.com' },
    { id: 8, name: 'yyy', email: 'jane@example.com' },
    { id: 9, name: 'yyy', email: 'jane@example.com' },
    { id: 10, name: 'yyy', email: 'jane@example.com' },
    { id: 11, name: 'yyy', email: 'jane@example.com' },
    { id: 12, name: 'yyy', email: 'jane@example.com' },
    { id: 13, name: 'yyy', email: 'jane@example.com' },
];


// Hàm mô phỏng yêu cầu API để lấy danh sách người dùng
const getList = async (resource, params) => {
  return { data: users, total: users.length }; // Bổ sung thông tin tổng số bản ghi
};
// Hàm mô phỏng yêu cầu API để lấy thông tin người dùng theo ID
const getOne = async (resource, params) => {
    const user = users.find((user) => user.id === parseInt(params.id));
    return { data: user };
};

// Hàm mô phỏng yêu cầu API để tạo mới người dùng
const create = async (resource, params) => {
    const newUser = { ...params.data, id: users.length + 1 };
    users.push(newUser);
    return { data: newUser };
};

// Hàm mô phỏng yêu cầu API để cập nhật thông tin người dùng theo ID
const update = async (resource, params) => {
    const index = users.findIndex((user) => user.id === parseInt(params.id));
    users[index] = { ...users[index], ...params.data };
    return { data: users[index] };
};

// Hàm mô phỏng yêu cầu API để xóa người dùng theo ID
const remove = async (resource, params) => {
    const index = users.findIndex((user) => user.id === parseInt(params.id));
    users.splice(index, 1);
    return { data: params.previousData };
};

// Triển khai dataProvider
const dataProvider = {
    getList,
    getOne,
    create,
    update,
    remove,
};

export { dataProvider };
