import actionTypes from '../actions/actionType';

const initState = {
    authLoading: false,
    isAuthenticated: false,
    user: null,
};

function authReducer(state = initState, action) {
    const {payload} = action
    switch (action.type) {
        case actionTypes.SET_AUTH:
            return {
                ...state,
                authLoading: payload.authLoading,
                isAuthenticated: payload.isAuthenticated,
                user: { ...payload.user } || null,
            };
        case actionTypes.AUTH_LOADING:
            return {
                ...state,
                authLoading: payload.authLoading
            }

        default:
            return state;
    }
}

export default authReducer;
