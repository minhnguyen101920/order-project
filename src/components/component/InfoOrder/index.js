import { useEffect, useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

function InfoOrder({ show, setShow, setSubmit }) {
    const [info, setInfo] = useState({
        fullname: '',
        phone: '',
        address: '',
    });

    useEffect(() => {
        const infoPre = localStorage.getItem('info');
        if (infoPre) setInfo({ ...info, ...JSON.parse(infoPre) });
    }, []);

    const handleClose = () => setShow(false);
    const handleChange = (e) => {
        setInfo({
            ...info,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = () => {
  
        localStorage.setItem('info', JSON.stringify(info));
        setSubmit(true);
        setShow(false);
    };
    return (
        <div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Thông tin nhận hàng</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Họ và tên</Form.Label>
                            <Form.Control
                                type="Họ và tên"
                                placeholder="Nhập tên người nhận"
                                value={info.fullname}
                                onChange={handleChange}
                                name="fullname"
                            />
                        </Form.Group>

                        <Form.Group controlId="formBasicPhone">
                            <Form.Label>Số điện thoại</Form.Label>
                            <Form.Control
                                type="phone"
                                placeholder="Nhập số điện thoại người nhận"
                                value={info.phone}
                                onChange={handleChange}
                                name="phone"
                            />
                        </Form.Group>
                        <Form.Group controlId="formBasicAddress">
                            <Form.Label>Địa chỉ</Form.Label>
                            <Form.Control
                                type="Address"
                                placeholder="Nhập địa chỉ người nhận"
                                value={info.address}
                                onChange={handleChange}
                                name="address"
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={handleSubmit}>
                        Lưu đơn hàng
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default InfoOrder;
