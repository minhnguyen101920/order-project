import { FaTrashAlt } from 'react-icons/fa';
import Input from '../Input';
import './Style.scss';
import { useEffect, useRef, useState } from 'react';
import { upload } from '../../../services/orderService';

function OrderItem({ submit, _key, handleRemove }) {
    // console.log(_key);
    const [dataOrder, setDataOrder] = useState({
        // image: '',
        link: '',
        color: '',
        size: '',
        price: '',
        quantity: '',
        linkImage: '',
        note: '',
        productName: '',
    });

    // console.log(submit)
    useEffect(()=>{
        if(submit){
            const info = JSON.parse(localStorage.getItem('info'));
            const data = {
                ...dataOrder,
                ...info
            }
            console.log(data);
            handleRemove(_key);
        }
    },[submit, _key])

    

    const refIMG = useRef();

    const handleChange = (e) => {
        setDataOrder((pre) => {
            return {
                ...pre,
                [e.target.name]: e.target.value,
            };
        });
    };

    const handleUpload = async (e) => {
        var file = e.target.files[0];
        if (file) {
            var formData = new FormData(); // Tạo đối tượng FormData để chứa dữ liệu tệp tin

            formData.append('file', file);

            try {
                const response = await upload(formData);
                const url = `${process.env.REACT_APP_API_URL}/${response.data.imagePath}`;
                refIMG.current.src = url;
            } catch (error) {}
        } else {
            refIMG.current.src = ''; // Nếu không chọn tệp tin, đặt src của thẻ img thành rỗng
        }
    };
    return (
        <div className="mb-5">
            <div className="d-flex justify-content-center align-items-center">
                <div className="mx-1 images-upload">
                    <input onChange={handleUpload} type="file" />
                    <span>Ảnh</span>
                    <img ref={refIMG} alt="" />
                </div>
                <div className="mx-1">
                    <Input
                        onChange={handleChange}
                        value={dataOrder.productName}
                        className="form-control"
                        name="productName"
                        placeholder="Tên sản phẩm"
                    />
                </div>
                <div className="mx-1">
                    <Input
                        onChange={handleChange}
                        value={dataOrder.link}
                        className="form-control"
                        name="link"
                        placeholder="Nhập link sản phẩm"
                    />
                </div>
                <div className="mx-1">
                    <Input
                        onChange={handleChange}
                        value={dataOrder.color}
                        className="form-control w-input"
                        name="color"
                        placeholder="Nhập màu"
                    />
                </div>
                <div className="mx-1">
                    <Input
                        onChange={handleChange}
                        value={dataOrder.size}
                        className="form-control w-input"
                        name="size"
                        placeholder="Nhập size"
                    />
                </div>
                <div className="mx-1">
                    <Input
                        onChange={handleChange}
                        value={dataOrder.price}
                        className="form-control w-input"
                        name="price"
                        placeholder="Giá"
                    />
                </div>
                <div className="mx-1">
                    <Input
                        onChange={handleChange}
                        value={dataOrder.quantity}
                        className="form-control w-input"
                        name="quantity"
                        placeholder="Số lượng"
                        type="number"
                    />
                </div>
                <div className="mx-1">
                    <Input
                        onChange={handleChange}
                        value={dataOrder.linkImage}
                        className="form-control"
                        name="linkImage"
                        placeholder="Dán link ảnh"
                    />
                </div>
                <div className="mx-1">
                    <Input
                        onChange={handleChange}
                        value={dataOrder.note}
                        className="form-control"
                        name="note"
                        placeholder="Ghi chú"
                    />
                </div>
                <FaTrashAlt className="w-icon mx-1" onClick={(e) => handleRemove(_key)} />
            </div>
        </div>
    );
}

export default OrderItem;
