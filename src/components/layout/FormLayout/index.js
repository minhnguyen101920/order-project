import classNames from 'classnames/bind';
import styles from './FormLayout.module.scss';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Navigate, useNavigate } from 'react-router-dom';

const cx = classNames.bind(styles);

function FormLayout({ children }) {
    const { isAuthenticated, authLoading } = useSelector((state) => state.auth);
    // const navigate = useNavigate();
    // useEffect(() => {
    //     if (isAuthenticated) navigate('/');
    // }, [isAuthenticated]);

    if (authLoading) return <h1>Loading</h1>;
    else if (isAuthenticated) {
        return <Navigate to={'/'}/>
        // navigate('/');
    } else return <div className={cx('wrapper')}>{children}</div>;
}

export default FormLayout;
